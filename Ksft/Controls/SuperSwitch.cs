﻿using Microsoft.Graphics.Canvas;
using Microsoft.Graphics.Canvas.Text;
using Microsoft.Graphics.Canvas.UI.Composition;
using Microsoft.Toolkit.Uwp.UI.Controls;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Numerics;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Graphics.DirectX;
using Windows.UI;
using Windows.UI.Composition;
using Windows.UI.Composition.Interactions;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Documents;
using Windows.UI.Xaml.Hosting;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Shapes;

// The Templated Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234235

namespace Ksft.Controls
{
    [TemplatePart(Name = ValueTextPartName, Type = typeof(TextBlock))]
    [TemplatePart(Name = ContainerPartName, Type = typeof(Border))]
    [TemplatePart(Name = RectPartName, Type = typeof(Rectangle))]
    [TemplatePart(Name = FillerPartName, Type = typeof(Ellipse))]
    [TemplatePart(Name = BorderPartName, Type = typeof(Border))]
    [TemplatePart(Name = PanelPartName, Type = typeof(DropShadowPanel))]
    public sealed class SuperSwitch : Control
    {
        // Template Parts.
        private const string ValueTextPartName = "PART_ValueText";
        private const string ContainerPartName = "PART_Container";
        private const string FillerPartName = "PART_Filler";
        private const string CanvasPartName = "PART_Canvas";
        private const string BorderPartName = "PART_Border";
        private const string RectPartName = "PART_Rect";
        private const string PanelPartName = "PART_ShadowPanel";

        private Duration _duration = new Duration(TimeSpan.FromSeconds(1));

        bool _pointerIn = false;

        private Color _inColor = Colors.MistyRose;

        #region Dependency Property Registrations

        public static readonly DependencyProperty ValueProperty =
            DependencyProperty.Register("Value", typeof(double), typeof(SuperSwitch), new PropertyMetadata(0.0, OnValueChanged));

        private static void OnValueChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            OnValueChanged(d);
        }

        private static void OnValueChanged(DependencyObject d)
        {
            //todo: redraw
        }

        public static readonly DependencyProperty TextProperty =
            DependencyProperty.Register("Text", typeof(string), typeof(SuperSwitch), new PropertyMetadata(string.Empty));
        
        public static readonly DependencyProperty ValueBrushProperty =
            DependencyProperty.Register("ValueBrush", typeof(Brush), typeof(SuperSwitch), new PropertyMetadata(new SolidColorBrush(Colors.White)));

        public static readonly DependencyProperty TextBrushProperty =
            DependencyProperty.Register("TextBrush", typeof(Brush), typeof(SuperSwitch), new PropertyMetadata(new SolidColorBrush(Colors.White)));

        public static readonly DependencyProperty ValueStringFormatProperty =
            DependencyProperty.Register("ValueStringFormat", typeof(string), typeof(SuperSwitch), new PropertyMetadata("N0"));

        public static readonly DependencyProperty FillRadiusProperty =
            DependencyProperty.Register("FillRadius", typeof(double), typeof(SuperSwitch), new PropertyMetadata(0.0, OnRadiusChanged));

        public static readonly DependencyProperty FillXProperty =
            DependencyProperty.Register("FillX", typeof(double), typeof(SuperSwitch), new PropertyMetadata(0.0));
        public static readonly DependencyProperty FillYProperty =
            DependencyProperty.Register("FillY", typeof(double), typeof(SuperSwitch), new PropertyMetadata(0.0));

        public static readonly DependencyProperty ItemWidthProperty =
            DependencyProperty.Register("ItemWidth", typeof(double), typeof(SuperSwitch), new PropertyMetadata(90.0));
        public static readonly DependencyProperty ItemLeftProperty =
            DependencyProperty.Register("ItemLeft", typeof(double), typeof(SuperSwitch), new PropertyMetadata(10.0));

        public double ItemWidth
        {
            get { return (double)GetValue(ItemWidthProperty); }
            set { SetValue(ItemWidthProperty, value); }
        }

        public double ItemLeft
        {
            get { return (double)GetValue(ItemLeftProperty); }
            set { SetValue(ItemLeftProperty, value); }
        }

        #endregion Dependency Property Registrations

        #region Composition API fields.

        private Compositor _compositor;
        private ContainerVisual _root;
        private SpriteVisual _needle;
        private InteractionTracker _tracker;
        private VisualInteractionSource _source;

        #endregion

        public static void OnRadiusChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var that = (d as SuperSwitch);
            //Debug.WriteLine($"FillRadiusPropertyChanged: {e.OldValue} -> {e.NewValue}, FillRadius = ${that.FillRadius}");
            that.RedrawBrush();
        }

        public double FillRadius
        {
            get { return (double)GetValue(FillRadiusProperty); }
            set { SetValue(FillRadiusProperty, value); }
        }

        public double FillX
        {
            get { return (double)GetValue(FillXProperty); }
            set { SetValue(FillXProperty, value); }
        }
        public double FillY
        {
            get { return (double)GetValue(FillYProperty); }
            set { SetValue(FillYProperty, value); }
        }

        /// <summary>
        /// Gets or sets the current value.
        /// </summary>
        public double Value
        {
            get { return (double)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }

        /// <summary>
        /// Gets or sets the unit measure.
        /// </summary>
        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        public SuperSwitch()
        {
            this.DefaultStyleKey = typeof(SuperSwitch);
        }


        protected override void OnApplyTemplate()
        {
            //var container = this.GetTemplateChild(CanvasPartName) as Canvas;
            //var canvas = this.GetTemplateChild(CanvasPartName) as Canvas;
            var container = this.GetTemplateChild(BorderPartName) as Border;
            _root = container.GetVisual();
            _compositor = _root.Compositor;

            this.PointerEntered += Container_PointerEntered;
            this.PointerExited += Canvas_PointerExited;

            //SetupInput();

            DrawBrush();

            OnValueChanged(this);
            base.OnApplyTemplate();
        }

        private void Canvas_PointerExited(object sender, PointerRoutedEventArgs e)
        {
            _pointerIn = false;
            Debug.WriteLine("_pointerIn = false;");
            //var filler = this.GetTemplateChild(FillerPartName) as Ellipse;
            //var canvas = this.GetTemplateChild(CanvasPartName) as Canvas;
            //var animation = (Storyboard)canvas.Resources["FillANIMATION"];
            //animation.Stop();
            //var transform = filler.RenderTransform as ScaleTransform;
            //transform.ScaleX = 0;
            //transform.ScaleY = 0;
        }

        private void SetupInput()
        {
            var bord = this.GetTemplateChild(BorderPartName) as Border;
            var filler = this.GetTemplateChild(FillerPartName) as Ellipse;
            var panel = this.GetTemplateChild(PanelPartName) as DropShadowPanel;
            var movement = panel.RenderTransform as TranslateTransform;
            

            _tracker = InteractionTracker.Create(_compositor);
            _tracker.MinPosition = new Vector3(0f);
            _tracker.MaxPosition = new Vector3(3000f);

            _source = VisualInteractionSource.Create(bord.GetVisual());
            _source.ManipulationRedirectionMode =
                VisualInteractionSourceRedirectionMode.CapableTouchpadOnly;
            _source.PositionYSourceMode = InteractionSourceMode.EnabledWithInertia;
            _tracker.InteractionSources.Add(_source);

            var scrollExp = _compositor.CreateExpressionAnimation("-tracker.Position.X");
            scrollExp.SetReferenceParameter("tracker", _tracker);
            //ElementCompositionPreview.GetElementVisual(this).StartAnimation("Value", scrollExp);
        }

        CompositionSurfaceBrush _drawingBrush;
        CanvasDevice _device;
        CompositionGraphicsDevice _compositionGraphicsDevice;
        CompositionDrawingSurface _drawingSurface;

        Storyboard _sboard = new Storyboard();
        DoubleAnimation _fillAnimation;

        private void DrawBrush()
        {
            var container = this.GetTemplateChild(BorderPartName) as Border;

            _device = CanvasDevice.GetSharedDevice();
            _compositionGraphicsDevice = CanvasComposition.CreateCompositionGraphicsDevice(_compositor, _device);

            _drawingBrush = _compositor.CreateSurfaceBrush();
            _drawingSurface = _compositionGraphicsDevice.CreateDrawingSurface(new Size(256, 256), DirectXPixelFormat.B8G8R8A8UIntNormalized, DirectXAlphaMode.Premultiplied);

            _drawingBrush.Surface = _drawingSurface;

            _needle = _compositor.CreateSpriteVisual();
            _needle.Brush = _drawingBrush;
            _needle.Size = new Vector2(200f, 200f);
            _needle.CenterPoint = new Vector3(_needle.Size.X / 2, _needle.Size.Y / 2, 0);

            ElementCompositionPreview.SetElementChildVisual(container, _needle);

            RedrawBrush();

            _fillAnimation = new DoubleAnimation()
            {
                From = 0d,
                To = 400d,
                Duration = _duration,
                EnableDependentAnimation = true
            };
            _sboard.Children.Add(_fillAnimation);
            Storyboard.SetTargetProperty(_fillAnimation, "FillRadius");
            Storyboard.SetTarget(_sboard, this);
        }

        private void RedrawBrush()
        {
            using (var ds = CanvasComposition.CreateDrawingSession(_drawingSurface))
            {
                ds.Clear(Colors.Transparent);
                ds.FillCircle((float)FillX, (float)FillY, (float)FillRadius, _inColor);
            }
        }

        private void Container_PointerEntered(object sender, PointerRoutedEventArgs e)
        {
            //var filler = this.GetTemplateChild(FillerPartName) as Ellipse;
            //var canvas = this.GetTemplateChild(CanvasPartName) as Canvas;

            if (_pointerIn)
                return;

            _pointerIn = true;

            var container = this.GetTemplateChild(BorderPartName) as Border;
            //determine mouse position
            var point = e.GetCurrentPoint(container);

            FillX = point.Position.X;
            FillY = point.Position.Y;

            //var animation = (Storyboard)canvas.Resources["FillANIMATION"];
            ////animation.Completed += Animation_Completed;
            //animation.Begin();
            Debug.WriteLine($"Animation started! X:{FillX}, Y:{FillY}");

            //var animation = _compositor.CreateVector2KeyFrameAnimation();
            //var easing = _compositor.CreateLinearEasingFunction();
            //animation.InsertKeyFrame(0.0f, new Vector2(0, 0));
            //animation.InsertKeyFrame(1.0f, new Vector2(90, 90), easing);
            //animation.Duration = TimeSpan.FromMilliseconds(1000);
            //animation.IterationBehavior = AnimationIterationBehavior.Forever;
            //_needle.Offset = new Vector3((float)FillX, (float)FillY, 0f);
            //_needle.Offset = new Vector3((float)FillX, (float)FillY, 0f);
            //_needle.StartAnimation(nameof(_needle.Size), animation);

            _sboard.Begin();
        }

        private void Animation_Completed(object sender, object e)
        {
            Debug.WriteLine("Hello!");
        }
    }
}
