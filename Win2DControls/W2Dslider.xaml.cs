﻿using Microsoft.Graphics.Canvas.Brushes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Win2DControls
{
    public sealed partial class W2Dslider : UserControl
    {
        public W2Dslider()
        {
            this.InitializeComponent();

            
        }

        public static readonly DependencyProperty ValueProperty =
            DependencyProperty.Register("Value", typeof(double), typeof(W2Dslider), new PropertyMetadata(0.0, OnValueChanged));

        private static void OnValueChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            OnValueChanged(d);
        }

        private static void OnValueChanged(DependencyObject d)
        {
            //todo: redraw
        }

        /// <summary>
        /// Gets or sets the current value.
        /// </summary>
        public double Value
        {
            get { return (double)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }

        // Background
        private Vector2 LeftPoint { get; set; }
        private float Radius { get; set; }
        private Vector2 RightPoint { get; set; }
        private ICanvasBrush BackgroundBrush { get; set; }
        private ICanvasBrush StrokeBrush { get; set; }
        private Rect CenterFill { get; set; }
        private Rect FullRect { get; set; }

        // Point
        private float InnerRadius { get; set; }
        private float PointX { get; set; }


        private void CanvasControl_Draw(Microsoft.Graphics.Canvas.UI.Xaml.CanvasControl sender, Microsoft.Graphics.Canvas.UI.Xaml.CanvasDrawEventArgs args)
        {
            RecalculatePoint();
            using (var session = args.DrawingSession)
            {
                DrawBackground(session);
                DrawPoint(session);
            }
        }

        internal void RecalculatePoint()
        {
            PointX = (float)(Radius + CenterFill.Width * Value);
        }

        private void DrawBackground(Microsoft.Graphics.Canvas.CanvasDrawingSession session)
        {
            session.FillCircle(LeftPoint, Radius, BackgroundBrush);
            session.FillRectangle(CenterFill, BackgroundBrush);
            session.FillCircle(RightPoint, Radius, BackgroundBrush);
            session.DrawRoundedRectangle(FullRect, Radius, Radius, StrokeBrush);
        }

        private void DrawPoint(Microsoft.Graphics.Canvas.CanvasDrawingSession session)
        {
            session.FillCircle(PointX, Radius, InnerRadius, StrokeBrush);
        }

        private void CanvasControl_CreateResources(Microsoft.Graphics.Canvas.UI.Xaml.CanvasControl sender, Microsoft.Graphics.Canvas.UI.CanvasCreateResourcesEventArgs args)
        {
            Radius = (float)ActualHeight / 2;
            LeftPoint = new Vector2(Radius, Radius);
            RightPoint = new Vector2((float)(ActualWidth - Radius), Radius);
            BackgroundBrush = new CanvasSolidColorBrush(sender, Colors.Chocolate);
            StrokeBrush = new CanvasSolidColorBrush(sender, Colors.DarkSlateBlue);
            CenterFill = new Rect(LeftPoint.X, 0, RightPoint.X - LeftPoint.X, Radius * 2);
            FullRect = new Rect(0, 0, (float)ActualWidth, (float)ActualHeight);
            InnerRadius = 0.8f * Radius;
        }
    }
}
